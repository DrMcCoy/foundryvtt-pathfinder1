{
  "name": "Schools of Magic",
  "pages": [
    {
      "sort": 100000,
      "name": "Abjuration",
      "type": "text",
      "_id": "JJkYYcnyKbmNIrrO",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Abjurations are protective spells. They create physical or magical barriers, negate magical or physical abilities, harm trespassers, or even banish the subject of the spell to another plane of existence.</p><p>If one abjuration spell is active within 10 feet of another for 24 hours or more, the magical fields interfere with each other and create barely visible energy fluctuations. The DC to find such spells with the Perception skill drops by 4.</p><p>If an abjuration creates a barrier that keeps certain types of creatures at bay, that barrier cannot be used to push away those creatures. If you force the barrier against such a creature, you feel a discernible pressure against the barrier. If you continue to apply pressure, you end the spell.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 200000,
      "name": "Conjuration",
      "type": "text",
      "_id": "8doqy0wUOpWXlXVq",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Each conjuration spell belongs to one of five subschools. Conjurations transport creatures from another plane of existence to your plane (calling); create objects or effects on the spot (creation); heal (healing); bring manifestations of objects, creatures, or forms of energy to you (summoning); or transport creatures or objects over great distances (teleportation). Creatures you conjure usually—but not always—obey your commands.</p><p>A creature or object brought into being or transported to your location by a conjuration spell cannot appear inside another creature or object, nor can it appear f loating in an empty space. It must arrive in an open location on a surface capable of supporting it.</p><p>The creature or object must appear within the spell's range, but it does not have to remain within the range.</p><h2>Calling</h2><p>A calling spell transports a creature from another plane to the plane you are on. The spell grants the creature the one-time ability to return to its plane of origin, although the spell may limit the circumstances under which this is possible. Creatures who are called actually die when they are killed; they do not disappear and reform, as do those brought by a summoning spell (see below). The duration of a calling spell is instantaneous, which means that the called creature can't be dispelled.</p><h2>Creation</h2><p>A creation spell manipulates matter to create an object or creature in the place the spellcaster designates. If the spell has a duration other than instantaneous, magic holds the creation together, and when the spell ends, the conjured creature or object vanishes without a trace. If the spell has an instantaneous duration, the created object or creature is merely assembled through magic. It lasts indefinitely and does not depend on magic for its existence.</p><h2>Healing</h2><p>Certain divine conjurations heal creatures or even bring them back to life.</p><h2>Summoning</h2><p>A summoning spell instantly brings a creature or object to a place you designate. When the spell ends or is dispelled, a summoned creature is instantly sent back to where it came from, but a summoned object is not sent back unless the spell description specifically indicates this. A summoned creature also goes away if it is killed or if its hit points drop to 0 or lower, but it is not really dead. It takes 24 hours for the creature to reform, during which time it can't be summoned again.</p><p>When the spell that summoned a creature ends and the creature disappears, all the spells it has cast expire. A summoned creature cannot use any innate summoning abilities it may have.</p><h2>Teleportation</h2><p>A teleportation spell transports one or more creatures or objects a great distance. The most powerful of these spells can cross planar boundaries. Unlike summoning spells, the transportation is (unless otherwise noted) one-way and not dispellable.</p><p>Teleportation is instantaneous travel through the Astral Plane. Anything that blocks astral travel also blocks teleportation.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 300000,
      "name": "Divination",
      "type": "text",
      "_id": "nEmQBCf2hoztaP7R",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Divination spells enable you to learn secrets long forgotten, predict the future, find hidden things, and foil deceptive spells.</p><p>Many divination spells have cone-shaped areas. These move with you and extend in the direction you choose. The cone defines the area that you can sweep each round. If you study the same area for multiple rounds, you can often gain additional information, as noted in the descriptive text for the spell.</p><h2>Scrying</h2><p>A scrying spell creates an invisible magical sensor that sends you information. Unless noted otherwise, the sensor has the same powers of sensory acuity that you possess. This level of acuity includes any spells or effects that target you, but not spells or effects that emanate from you. The sensor, however, is treated as a separate, independent sensory organ of yours, and thus functions normally even if you have been blinded or deafened, or otherwise suffered sensory impairment.</p><p>A creature can notice the sensor by making a Perception check with a DC 20 + the spell level. The sensor can be dispelled as if it were an active spell.</p><p>Lead sheeting or magical protection blocks a scrying spell, and you sense that the spell is blocked.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 400000,
      "name": "Enchantment",
      "type": "text",
      "_id": "w0pgoCUgrIfXNfRo",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Enchantment spells affect the minds of others, influencing or controlling their behavior. All enchantments are mind-affecting spells. Two subschools of enchantment spells grant you influence over a subject creature.</p><h2>Charm</h2><p>A charm spell changes how the subject views you, typically making it see you as a good friend.</p><h2>Compulsion</h2><p>A compulsion spell forces the subject to act in some manner or changes the way its mind works. Some compulsion spells determine the subject’s actions or the effects on the subject, others allow you to determine the subject’s actions when you cast the spell, and still others give you ongoing control over the subject.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 500000,
      "name": "Evocation",
      "type": "text",
      "_id": "QfiMOKtMPORWer6c",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Evocation spells manipulate magical energy or tap an unseen source of power to produce a desired end. In effect, an evocation draws upon magic to create something out of nothing. Many of these spells produce spectacular effects, and evocation spells can deal large amounts of damage.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 600000,
      "name": "Illusion",
      "type": "text",
      "_id": "hF0yM0ZyJAApxrcX",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Illusion spells deceive the senses or minds of others. They cause people to see things that are not there, not see things that are there, hear phantom noises, or remember things that never happened.</p><p><b>Saving Throws and Illusions (Disbelief):</b> Creatures encountering an illusion usually do not receive saving throws to recognize it as illusory until they study it carefully or interact with it in some fashion.</p><p>A successful saving throw against an illusion reveals it to be false, but a figment or phantasm remains as a translucent outline. A failed saving throw indicates that a character fails to notice something is amiss.</p><p>A character faced with proof that an illusion isn’t real needs no saving throw. If any viewer successfully disbelieves an illusion and communicates this fact to others, each such viewer gains a saving throw with a +4 bonus.</p><h2>Figment</h2><p>A figment spell creates a false sensation. Those who perceive the figment perceive the same thing, not their own slightly different versions of the figment. It is not a personalized mental impression. Figments cannot make something seem to be something else. A figment that includes audible effects cannot duplicate intelligible speech unless the spell description specifically says it can. If intelligible speech is possible, it must be in a language you can speak. If you try to duplicate a language you cannot speak, the figment produces gibberish. Likewise, you cannot make a visual copy of something unless you know what it looks like (or copy another sense exactly unless you have experienced it).</p><p>Because figments and glamers are unreal, they cannot produce real effects the way that other types of illusions can. Figments and glamers cannot cause damage to objects or creatures, support weight, provide nutrition, or provide protection from the elements. Consequently, these spells are useful for confounding foes, but useless for attacking them directly.</p><p>A figment’s AC is equal to 10 + its size modifier.</p><h2>Glamer</h2><p>A glamer spell changes a subject’s sensory qualities, making it look, feel, taste, smell, or sound like something else, or even seem to disappear. Pattern: Like a figment, a pattern spell creates an image that others can see, but a pattern also affects the minds of those who see it or are caught in it. All patterns are mind-affecting spells.</p><h2>Phantasm</h2><p>A phantasm spell creates a mental image that usually only the caster and the subject (or subjects) of the spell can perceive. This impression is totally in the minds of the subjects. It is a personalized mental impression, all in their heads and not a fake picture or something that they actually see. Third parties viewing or studying the scene don’t notice the phantasm. All phantasms are mind-affecting spells.</p><h2>Shadow</h2><p>A shadow spell creates something that is partially real from extradimensional energy. Such illusions can have real effects. Damage dealt by a shadow illusion is real.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 700000,
      "name": "Necromancy",
      "type": "text",
      "_id": "oh6BYHvlggvyl2HB",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Necromancy spells manipulate the power of death, unlife, and the life force. Spells involving undead creatures make up a large part of this school.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 800000,
      "name": "Transmutation",
      "type": "text",
      "_id": "4NbQvoXWWaWiNbvd",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Transmutation spells change the properties of some creature, thing, or condition.</p><h2>Polymorph</h2><p>A polymorph spell transforms your physical body to take on the shape of another creature. While these spells make you appear to be the creature, granting you a +10 bonus on Disguise skill checks, they do not grant you all of the abilities and powers of the creature. Each polymorph spell allows you to assume the form of a creature of a specific type, granting you a number of bonuses to your ability scores and a bonus to your natural armor. In addition, each polymorph spell can grant you a number of other benefits, including movement types, resistances, and senses. If the form you choose grants these benefits, or a greater ability of the same type, you gain the listed benefit. If the form grants a lesser ability of the same type, you gain the lesser ability instead. Your base speed changes to match that of the form you assume. If the form grants a swim or burrow speed, you maintain the ability to breathe if you are swimming or burrowing. The DC for any of these abilities equals your DC for the polymorph spell used to change you into that form.</p><p>In addition to these benefits, you gain any of the natural attacks of the base creature, including proficiency in those attacks. These attacks are based on your base attack bonus, modified by your Strength or Dexterity as appropriate, and use your Strength modifier for determining damage bonuses.</p><p>If a polymorph spell causes you to change size, apply the size modifiers appropriately, changing your armor class, attack bonus, Combat Maneuver Bonus, and Stealth skill modifiers. Your ability scores are not modified by this change unless noted by the spell.</p><p>Unless otherwise noted, polymorph spells cannot be used to change into specific individuals. Although many of the fine details can be controlled, your appearance is always that of a generic member of that creature's type. Polymorph spells cannot be used to assume the form of a creature with a template or an advanced version of a creature.</p><p>When you cast a polymorph spell that changes you into a creature of the animal, dragon, elemental, magical beast, plant, or vermin type, all of your gear melds into your body. Items that provide constant bonuses and do not need to be activated continue to function while melded in this way (with the exception of armor and shield bonuses, which cease to function). Items that require activation cannot be used while you maintain that form. While in such a form, you cannot cast any spells that require material components (unless you have the Eschew Materials or Natural Spell feat), and can only cast spells with somatic or verbal components if the form you choose has the capability to make such movements or speak, such as a dragon. Other polymorph spells might be subject to this restriction as well, if they change you into a form that is unlike your original form (subject to GM discretion). If your new form does not cause your equipment to meld into your form, the equipment resizes to match your new size.</p><p>While under the effects of a polymorph spell, you lose all extraordinary and supernatural abilities that depend on your original form (such as keen senses, scent, and darkvision), as well as any natural attacks and movement types possessed by your original form. You also lose any class features that depend upon form, but those that allow you to add features (such as sorcerers that can grow claws) still function. While most of these should be obvious, the GM is the final arbiter of what abilities depend on form and are lost when a new form is assumed. Your new form might restore a number of these abilities if they are possessed by the new form.</p><p>You can only be affected by one polymorph spell at a time. If a new polymorph spell is cast on you (or you activate a polymorph effect, such as wild shape), you can decide whether or not to allow it to affect you, taking the place of the old spell. In addition, other spells that change your size have no effect on you while you are under the effects of a polymorph spell.</p><p>If a polymorph spell is cast on a creature that is smaller than Small or larger than Medium, first adjust its ability scores to one of these two sizes using the following table before applying the bonuses granted by the polymorph spell.</p><table><thead><tr><th>Creature's Original Size</th><th>Str</th><th>Dex</th><th>Con</th><th>Adjusted Size</th></tr></thead><tbody><tr><td>Fine</td><td>+6</td><td>-6</td><td>-</td><td>Small</td></tr><tr><td>Diminutive</td><td>+6</td><td>-4</td><td>-</td><td>Small</td></tr><tr><td>Tiny</td><td>+4</td><td>-2</td><td>-</td><td>Small</td></tr><tr><td>Large</td><td>-4</td><td>+2</td><td>-2</td><td>Medium</td></tr><tr><td>Huge</td><td>-8</td><td>+4</td><td>-4</td><td>Medium</td></tr><tr><td>Gargantuan</td><td>-12</td><td>+4</td><td>-6</td><td>Medium</td></tr><tr><td>Colossal</td><td>-16</td><td>+4</td><td>-8</td><td>Medium</td></tr></tbody></table>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    },
    {
      "sort": 900000,
      "name": "Universal",
      "type": "text",
      "_id": "ahBEpooINXOmwsKj",
      "title": {
        "show": true,
        "level": 1
      },
      "image": {},
      "text": {
        "format": 1,
        "content": "<p>Almost every spell belongs to one of eight schools of magic. A school of magic is a group of related spells that work in similar ways. A small number of spells (arcane mark, limited wish, permanency, prestidigitation, and wish) are universal, belonging to no school.</p>",
        "markdown": ""
      },
      "video": {
        "controls": true,
        "volume": 0.5
      },
      "src": null,
      "system": {},
      "ownership": {
        "default": -1
      },
      "flags": {}
    }
  ],
  "sort": 0,
  "_id": "7fYYPFNPVJUrWRfA"
}
